package com.phil.midterm;

public class Pokemon {
    private String name;
    private int damage;
    private int hp;
    private String type;

public Pokemon(String name, int damage, int hp, String type) {
    this.name = name;
    this.damage = damage;
    this.hp = hp;
    this.type = type;
    }
public Pokemon() {}
    public String getName() {
        return name;
    }
    public int getDamage() {
        return damage;
    }
    public int getHp() {
        return hp;
    }
    public String getType() {
        return type;
    }
}