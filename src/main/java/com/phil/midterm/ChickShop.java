package com.phil.midterm;

import java.util.Scanner;

public class ChickShop {
    static void printChick() {
        System.out.println("---Chick---");
        System.out.println("Red");
        System.out.println("Yellow");
        System.out.println("White");
        System.out.println("Black");
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Chick Red = new Chick("Red", 1, 40);
        Chick Yellow = new Chick("Yellow", 4, 80);
        Chick White = new Chick("White", 6, 120);
        Chick Black = new Chick("Black", 8, 180);

        printChick();
        System.out.println("Please Select Chick: ");
        String select = sc.nextLine();
        if (select.toLowerCase().equals("red")) {
            System.out.println("You Select is red");
            System.out.println(" Color is " + Red.getColor() + "| Old " + Red.getOld() + " years " + "| Price "
                    + Red.getPrice() + " Bath ");
            System.out.println("How many you want ?");
            int Amount = sc.nextInt();
            int price = Red.getPrice() * Amount;
            System.out.println("Total: " + price + " Bath ");
        } else if (select.toLowerCase().equals("yellow")) {
            System.out.println("You Select is Yellow");
            System.out.println(" Color is " + Yellow.getColor() + "| Old " + Yellow.getOld() + " years " + "| Price "
                    + Yellow.getPrice() + " Bath ");
            System.out.println("How many you want ?");
            int Amount = sc.nextInt();
            int price = Yellow.getPrice() * Amount;
            System.out.println("Total: " + price + " Bath ");
        } else if (select.toLowerCase().equals("white")) {
            System.out.println("You Select is White");
            System.out.println(" Color is " + White.getColor() + "| Old " + White.getOld() + " years " + "| Price "
                    + White.getPrice() + " Bath ");
            System.out.println("How many you want ?");
            int Amount = sc.nextInt();
            int price = White.getPrice() * Amount;
            System.out.println("Total: " + price + " Bath ");
        } else if (select.toLowerCase().equals("black")) {
            System.out.println("You Select is Black");
            System.out.println(" Color is " + Black.getColor() + "| Old " + Black.getOld() + " years " + "| Price "
                    + Black.getPrice() + " Bath ");
            System.out.println("How many you want ?");
            int Amount = sc.nextInt();
            int price = Black.getPrice() * Amount;
            System.out.println("Total: " + price + " Bath ");
        }
        else {
            System.exit(0);
        }
    }
}
