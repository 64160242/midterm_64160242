package com.phil.midterm;

public class Chick {
    private int old;
    private String color;
    private int price;

    public Chick(String color, int old, int price) {
        this.old = old;
        this.color = color;
        this.price = price;
    }

    public int getOld() {
        return old;
    }

    public String getColor() {
        return color;
    }

    public int getPrice() {
        return price;
    }
}
