package com.phil.midterm;

import java.util.Scanner;

public class PokemonBattle {
    static boolean check = true;
    static void printPokemon() {
        System.out.println("---Pokemon---");
        System.out.println("Pikachu");
        System.out.println("Fushigidane");
        System.out.println("Hitokage");
        System.out.println("Run away");
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Pokemon MyPokemon = new Pokemon();
        Pokemon Pikachu = new Pokemon("Pikachu", 20, 100, "Electirc");
        Pokemon Fushigidane = new Pokemon("Fushigidane", 30, 100, "Grass");
        Pokemon Hitokage = new Pokemon("Hitokage", 40, 100, "Fire");
        Pokemon Mewtwo = new Pokemon("Mewtwo", 20, 150, "Psychic"); 
        
        do {
            printPokemon();
            System.out.print("Please choose Your Pokemon : ");
            String select = sc.nextLine();
            if (select.toLowerCase().equals("pikachu")) {
                System.out.println("You Choose Pikachu");
                System.out.println(Pikachu.getName());
                System.out.println("Damage: " + Pikachu.getDamage());
                System.out.println("HP: " + Pikachu.getHp());
                System.out.println("Type: " + Pikachu.getType());
                MyPokemon = Pikachu;
                check = true;
            } else if (select.toLowerCase().equals("fushigidane")) {
                System.out.println("You Choose Fushigidane");
                System.out.println(Fushigidane.getName());
                System.out.println("Damage: " + Fushigidane.getDamage());
                System.out.println("HP: " + Fushigidane.getHp());
                System.out.println("Type: " + Fushigidane.getType());
                MyPokemon = Fushigidane;
                check = true;
            } else if (select.toLowerCase().equals("hitokage")) {
                System.out.println("You Choose Hitokage");
                System.out.println(Hitokage.getName());
                System.out.println("Damage: " + Hitokage.getDamage());
                System.out.println("HP: " + Hitokage.getHp());
                System.out.println("Type: " + Hitokage.getType());
                MyPokemon = Hitokage;
                check = true;
            } else if (select.toLowerCase().equals("run away")) {
                System.out.println("Run away");
                System.exit(0);
            } else {
                check = false; 
            }
        } while (check == false);
        System.out.println("Mewtwo appear!!!");
        System.out.print("Please Enter your hit : ");
        int hit = sc.nextInt();
        int Damage = MyPokemon.getDamage()*hit;
        int result = Damage - Mewtwo.getHp();
        if(result >= 0) {
            System.out.println("Win");
        }
        else {
            System.out.println("Lose");
        }
    }
}